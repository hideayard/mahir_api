<?php
// ini_set('display_errors', 0);
// ini_set('display_startup_errors', none);
error_reporting(E_ALL);


require_once ("tokenlogin.php");
require_once ('config/MysqliDb.php');
include_once ("config/db.php");
require_once ("jwt_token.php");

$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
// $myfile = fopen("cronlog.txt", "w") or die("Unable to open file!");

/*
$txt = "John Doe\n";
fwrite($myfile, $txt);
$txt = "Jane Doe\n";
fwrite($myfile, $txt);

*/

    try {
        $db->startTransaction();
        $expired =  ' 0 data expired';

        ///---------check data expired
        $db->where ( "expired < DATE_FORMAT('".(new \DateTime())->format('Y-m-d H:i:s') ."', '%Y-%m-%d %H:%i:%s')" );
        $db->where ( "status = 'BOOKED'" );
        $data = Array (
          'status' => 'EXPIRED',
          'remark' => 'DATA BOOKING TELAH EXPIRED, ANDA DAPAT BOOKING ULANG ATAU MENGHUBUNGI ADMIN UNTUK INFO LEBIH LANJUT',
        );
        $getexpired = $db->get('booking');
        //debug
        $debug[]=$getexpired;

        if($db->count >0)
        {
            foreach($getexpired as $key => $value)
            {
              $debug[] = $db->rawQuery('UPDATE jadwal set jml_peserta=jml_peserta-1 where id = ?', Array ($value["jadwal_id"]));
            }
            //re-select
            $db->where ( "expired < DATE_FORMAT('".(new \DateTime())->format('Y-m-d H:i:s') ."', '%Y-%m-%d %H:%i:%s')" );
            $db->where ( "status <> 'EXPIRED'" );
            if ($db->update ('booking', $data))
            {
                $jml = $db->count;
                $expired =  $jml . ' data expired';
                $debug[]=$expired;//debug
                $db->commit();
            }
            else
             {
                $db->rollback();
    
               $expired =  'update failed: ' . $db->getLastError();
             } 
        }
        
      /// --end check data expired
      $txt = json_encode( array("status" => true,"info" => $getexpired ,"messages" => "Check success!" , "expired" => $expired , "debug" => $debug ) );//,"debug" =>  $debug ) );
      $myfile = file_put_contents('cronlog.txt', (new \DateTime())->format('Y-m-d H:i:s')." - ".$txt.PHP_EOL , FILE_APPEND | LOCK_EX);
        echo $txt;
       
   } catch (Exception $e) {
        $txt = json_encode( array("status" => false,"info" => 'Caught exception '.$db->getMessage(),"messages" => "Terjadi Kesalahan!" ) );
        $myfile = file_put_contents('cronlog.txt', (new \DateTime())->format('Y-m-d H:i:s')." - ".$txt.PHP_EOL , FILE_APPEND | LOCK_EX);
        echo $txt;

   }
   
//    fclose($myfile);


?>