<?php
ini_set('display_errors', 1);
// ini_set('display_startup_errors', none);
//error_reporting(E_ALL);
error_reporting(0);
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

require_once ("tokenlogin.php");
require_once ('config/MysqliDb.php');
include_once ("config/db.php");
require_once ("jwt_token.php");

$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
$token = isset($_POST['token']) ? $_POST['token'] : ""; 
$id = isset($_POST['id']) ? intval($_POST['id']) : ""; 
$page = isset($_POST['page']) ? intval($_POST['page']) : 1; 
$totalPages = 1;
if ($token != "") {


    try {

        $vtoken = json_decode( verify_token($token) );

        if($vtoken->status)
        {
            if ($id != "") 
            {
                $db->where ('user_status', "1");
                $db->where ('user_id', $id );
                $results = $db->get("users");
                $cnt = 1;
            }
            else
            {
                $db->where ('user_status', "1");
                $db->pageLimit = 10;
                $results = $db->arraybuilder()->paginate("users", $page);
                $cnt = $db->getValue ("users", "count(*)");
                $totalPages = $db->totalPages;
            }



            if(count($results)>0)
            {
                echo json_encode( array("status" => true,"info" => $results ,"messages" => "Get Data Success" , "total"=>$cnt , "page"=>$page , "totalPage" => $totalPages  ) );
            }
            else
            {
                echo json_encode( array("status" => false,"info" => "No Data Found.!" ,"messages" => "No Data Found.!", "total"=> 0 , "page"=> 0 , "totalPage" => 0  ) );
            }

       } else {
        echo json_encode( array("status" => false,"info" => 'Invalid token',"messages" => "Invalid token!" ) );
       }
   } catch (Exception $e) {
      echo json_encode( array("status" => false,"info" => 'Caught exception '.$db->getMessage(),"messages" => "Terjadi Kesalahan!" ) );

   }
}
else
{
    echo json_encode( array("status" => false,"info" => "","messages" => "Token not found!" ) );
}

?>