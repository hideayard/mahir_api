<?php
// ini_set('display_errors', 0);
// ini_set('display_startup_errors', none);
// error_reporting(0);
error_reporting(E_ALL);
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

        require_once ("tokenlogin.php");
        require_once ("jwt_token.php");
        require_once ("customhelper.php");
      require_once ('config/MysqliDb.php');
      include_once ("config/db.php");
      include("config/functions.php");    

try{
    $db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
    
    $tanggal = isset($_POST['tanggal']) ? $_POST['tanggal'] : ""; 
    $mulai = isset($_POST['mulai']) ? $_POST['mulai'] : ""; 
    $selesai = isset($_POST['selesai']) ? $_POST['selesai'] : ""; 
    $metode = isset($_POST['metode']) ? $_POST['metode'] : 0; 
    $gender = isset($_POST['gender']) ? $_POST['gender'] : "LAKI-LAKI"; 

    $db->where ("j.status", 1);
    $debug=array();
    $db->where ("DATE(tanggal) > NOW()");

    if($tanggal!="")
    {
        $db->where ("DATE(tanggal) = DATE('".$tanggal."')");
        $debug[]=$tanggal;
    }
    else{
        $db->where ("DATE(tanggal) > NOW() + INTERVAL 2 DAY");
    }

    if($mulai!="" && $mulai!=0)
    {
        $debug[]=$mulai;
        $db->where ('j.jam_mulai', $mulai, ">=");
    }

    if($selesai!="" && $selesai!=0)
    {
        $debug[]=$selesai;
        $db->where ('j.jam_selesai', $selesai, "<=");
    }

    if($metode!=0)
    {
        $debug[]=$metode;
        $db->where ("j.id_metode", intval($metode) );
    }
    $db->where ("u.user_kelamin", $gender );

    $db->join("users u", "j.user_id=u.user_id", "INNER");
    $limit = 10;
    $jadwal = $db->get ("jadwal j", $limit, "u.user_nama as nama, j.id,DATE_FORMAT(j.tanggal, '%d %M %Y') as tanggal, j.jam_mulai, j.jam_selesai, j.metode, (j.kuota - j.jml_peserta) as kuota , j.harga, j.status" );
    // $jadwal = $db->get ("v_jadwal", $limit, "*" );
    // $jadwal = $db->get('jadwal'); //contains an Array of all users 
    $debug[]=$jadwal;
    if(count($jadwal)>0)
    {
        // $hasil = [];
        // foreach ($metode as $data) {
        //     $hasil[] = $data;
        // }
        echo json_encode( array("status" => true,"info" => $jadwal ,"messages" => "Get data success!" ,"debug" =>  $debug ) );
    }
    else
    {
        echo json_encode( array("status" => true,"info" => [] ,"messages" => "Jadwal tidak ditemukan!" ,"debug" =>  $debug ) );

    }
} catch (Exception $e) {
    echo json_encode( array("status" => false,"info" => "Terjadi Kesalahan","messages" => "Terjadi kesalahan" ,"e" => $e->getMessage() ) );
  
  }
?>