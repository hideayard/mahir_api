<?php

ini_set('display_errors', 0);
error_reporting(0);

// ini_set('display_startup_errors', none);
// //error_reporting(E_ALL);
// error_reporting(null);

session_start();

$token = isset($_POST['token']) ? str_replace('"','',$_POST['token']) : ""; 
try{

  // Check a POST is valid.
  if ($token != "") {
        
    require_once ("jwt_token.php");
    require_once ("customhelper.php");
      require_once ('config/MysqliDb.php');
      include_once ("config/db.php");
      $db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
      include("config/functions.php");    

      $vtoken = json_decode( verify_token($token) );

      if($vtoken->status)
      {

        $id_session = $vtoken->data->user_id;
        $tipe_session = $vtoken->data->user_tipe;
        $target_dir = "uploads/user/";
        $mode = isset($_POST['mode']) ? $_POST['mode'] : ""; 
        $tgl = (new \DateTime())->format('Y-m-d H:i:s');

        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : ""; 
        $user_name = isset($_POST['user_name']) ? $_POST['user_name'] : ""; 
        $user_pass = isset($_POST['user_pass']) ? $_POST['user_pass'] : ""; 
        $user_nama = isset($_POST['user_nama']) ? $_POST['user_nama'] : ""; 
        $user_status = isset($_POST['user_status']) ? $_POST['user_status'] : ""; 
        $user_email = isset($_POST['user_email']) ? $_POST['user_email'] : ""; 
        $user_kelamin = isset($_POST['user_kelamin']) ? $_POST['user_kelamin'] : ""; 
        $user_hp = isset($_POST['user_hp']) ? $_POST['user_hp'] : ""; 
        $user_tipe = isset($_POST['user_tipe']) ? $_POST['user_tipe'] : ""; 
        $user_foto = isset($_FILES["user_foto"]["name"]) ? $_FILES["user_foto"]["name"] : ""; 
        $user_ktp = isset($_FILES["user_ktp"]["name"]) ? $_FILES["user_ktp"]["name"] : ""; 
        $delete_user_photo = isset($_POST['delete_user_photo']) ? $_POST['delete_user_photo'] : ""; 

        $message = "Insert Sukses!!";
        $tgl = (new \DateTime())->format('Y-m-d H:i:s');
        $hasil_upload1 = null;
        
        $uploadOk =1 ;
      

        $data = Array (  );
        if($user_name!="")
        {
            $data += array('user_name' => $user_name);
        }
        if($user_pass!="")
        {
            $data += array('user_pass' => md5($user_pass));
        }
        if($user_nama!="")
        {
            $data += array('user_nama' => $user_nama);
        }
        if($user_email!="")
        {
            $data += array('user_email' => $user_email);
        }
        if($user_hp!="")
        {
            $data += array('user_hp' => $user_hp);
        }
        if($user_kelamin!="")
        {
            $data += array('user_kelamin' => $user_kelamin);
        } 
        if($user_tipe!="")
        {
            $data += array('user_tipe' => $user_tipe);
        }

        if($user_status!="")
        {
            $data += array('user_status' => $user_status);
        }
        $i=-1;
        if($user_foto != "" )
        { //harusnya foto lama di hapus dulu baru upload baru
          $hasil_upload1 = upload_files("user","user_foto",++$i);
          $uploadOk .= "<br>".$hasil_upload1["uploadOk"];
          $message .= "<br>".$hasil_upload1["message"];
          $user_foto = $hasil_upload1["file_name"];
          $data += array('user_foto' => $hasil_upload1["file_name"]);
        }

        if($user_ktp != "" )
        { //harusnya foto lama di hapus dulu baru upload baru
          $hasil_upload2 = upload_files("user","user_ktp",++$i);
          $uploadOk .= "<br>".$hasil_upload2["uploadOk"];
          $message .= "<br>".$hasil_upload2["message"];
          $user_ktp = $hasil_upload2["file_name"];
          $data += array('user_ktp' => $hasil_upload2["file_name"]);
          $data += array('user_status' => 1);

        }
      
        $hasil_eksekusi = false;

        if(isset($_POST['user_id']))
        {    
          if($mode == "delete" && $tipe_session=="ADMIN")
            {
              $db->where('user_id', $user_id);
              $hasil_eksekusi = $db->delete('users');
              $message = "Delete Success !!";
            }
            else
            {
              $data += array('user_modified_by' => $id_session);
              $data += array('user_modified_at' => $tgl);
              $data += array('user_is_deleted' => 0);
              if( $tipe_session=="ADMIN")
              {
                $db->where ('user_id', $user_id);
              }
              else
              {
                $db->where ('user_id', $id_session);
              }

              $hasil_eksekusi = $db->update ('users', $data);
              $message = "Update Success !!";

            }
            
            if ($hasil_eksekusi)
            {   
              echo json_encode( array("status" => true,"info" => $user_status,"messages" => $message ) );
            }//$db->count . ' records were updated';
            else
            {   
              echo json_encode( array("status" => false,"info" => 'update failed: ' . $db->getLastError(),"messages" => $message ) );

            }

          }
          else
          {  
            $message = "Insert Success";
              $data += array("user_id" => null);
              $data += array('user_created_by' => $id_session);
              $data += array('user_created_at' => $tgl);
            if($db->insert ('users', $data))
            {
              echo json_encode( array("status" => true,"info" => $user_status,"messages" => $message ) );
                      }
            else
            {
              $message = "Insert Fail";
              echo json_encode( array("status" => false,"info" => $db->getLastError(),"messages" => $message ) );
          
          
            }
          }
        }
        else
        {
          echo json_encode( array("status" => false,"info" => "token not valid","messages" => "token not valids!" ) );
        }
  }
  else
  {
    echo json_encode( array("status" => false,"info" => "request not valid","messages" => "request not valids!" ) );
  }
} catch (Exception $e) {
  echo json_encode( array("status" => false,"info" => "Terjadi Kesalahan","messages" => "Terjadi kesalahan" ,"e" => $e->getMessage() ) );

}
?>