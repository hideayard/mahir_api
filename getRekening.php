<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', none);
//error_reporting(E_ALL);
error_reporting(0);
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

require_once ("tokenlogin.php");
require_once ('config/MysqliDb.php');
include_once ("config/db.php");
require_once ("jwt_token.php");

$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
$token = isset($_POST['token']) ? $_POST['token'] : ""; 
$id = isset($_POST['id']) ? intval($_POST['id']) : ""; 

if ($token != "") {

    try {

        $vtoken = json_decode( verify_token($token) );

        if ($id != "" && $vtoken->status && $vtoken->status && $vtoken->data->user_tipe == "ADMIN") 
        {
            $db->where ('user_id', $id);
            $skill = $db->get('users_skill');
            $db->where ('user_id', $id);
            $rekening = $db->get('users_bank_account');
            $results = array("skill" => $skill,"rekening" => $rekening) ;
            echo json_encode( array("status" => true,"info" => $results,"messages" => "get data success" ) );

        }
        else
        {

            if($vtoken->status)
            {
              $db->where ('user_id', $vtoken->data->user_id);
              $skill = $db->get('users_skill');
              $db->where ('user_id', $vtoken->data->user_id);
              $rekening = $db->get('users_bank_account');
              $results = array("skill" => $skill,"rekening" => $rekening) ;
              echo json_encode( array("status" => true,"info" => $results,"messages" => "get data success" ) );
    
           } else {
            echo json_encode( array("status" => false,"info" => 'Invalid token',"messages" => "Invalid token!" ) );
           }


        }
       
   } catch (Exception $e) {
      echo json_encode( array("status" => false,"info" => 'Caught exception '.$db->getMessage(),"messages" => "Terjadi Kesalahan!" ) );

   }
}
else
{
    echo json_encode( array("status" => false,"info" => "","messages" => "Token not found!" ) );
}

?>