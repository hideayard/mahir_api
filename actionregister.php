<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', none);
//error_reporting(E_ALL);
error_reporting(0);
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
require_once ('config/MysqliDb.php');
include_once ("config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
include("config/functions.php");    

$tgl = (new \DateTime())->format('Y-m-d H:i:s');
$user_status = 0;

            $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : ""; 
            $user_name = isset($_POST['user_name']) ? $_POST['user_name'] : ""; 
            $user_pass = isset($_POST['user_pass']) ? $_POST['user_pass'] : ""; 
            $user_email = isset($_POST['user_email']) ? $_POST['user_email'] : ""; 
            $user_hp = isset($_POST['user_hp']) ? $_POST['user_hp'] : ""; 
            $user_kelamin = isset($_POST['user_kelamin']) ? $_POST['user_kelamin'] : "LAKI-LAKI"; 
            $user_kemampuan = isset($_POST['user_kemampuan']) ? $_POST['user_kemampuan'] : ""; 
            $user_tipe = isset($_POST['user_tipe']) ? $_POST['user_tipe'] : ""; 
    
            $db->startTransaction();

try{

    $message = "Insert Sukses!!";
    $tgl = (new \DateTime())->format('Y-m-d H:i:s');   
    $sql = "SELECT * FROM users where user_name='$user_name' ";
    $data = $db->rawQuery($sql);
    if(count($data)>0)
    {
        echo json_encode( array("status" => false,"info" => "error","messages" => "Username Already Exist" ) );
    }
    else
    {
              
      $data = Array ();
      if($user_name!="")
      {
          $data += array('user_name' => $user_name);
          $data += array("user_nama" => $user_name);

      }
      if($user_pass!="")
      {
          $data += array('user_pass' => md5($user_pass));
      }
      if($user_email!="")
      {
          $data += array('user_email' => $user_email);
      }
      if($user_hp!="")
      {
          $data += array('user_hp' => $user_hp);
      }

      if($user_kelamin=="LAKI-LAKI")
      {
          $data += array('user_foto' => "santriwan.jpg");
      }
      else
      {
        $data += array('user_foto' => "santriwati.jpg");

      }

      if($user_tipe=="USTADZ")
      {
        $data += array("user_tipe" => "USTADZ");
    }   
      else
      {
        $data += array("user_tipe" => "SANTRI");
      }
      
    
      $hasil_eksekusi = false;
    
        $data += array('user_kelamin' => $user_kelamin);
        $data += array("user_id" => null);
        $data += array('user_status' => 1);
        $data += array('user_created_by' => null);
        $data += array('user_created_at' => $tgl);
        $id = $db->insert ('users', $data);

        if( $id )
        {
            if($user_tipe == "SANTRI")
            {

                $db->commit();
                echo json_encode( array("status" => true,"info" => "success","messages" => "Insert Success", "debug" => $id ) );
            }
            else
            {

                $db->where ('status', 1);
                $db->where('nama', $user_kemampuan, 'IN');
                $results = $db->get('metode');
                $dataSkills = Array();

                foreach($results as $value)
                {

                    $dataSkills[] = array("nama" => $value["label"],"metode_id" => $value["id"],"user_id" => $id ,"created_at" => $tgl ,"created_by" => $id ,"modified_at" => $tgl ,"modified_by" => $id );
                
                }

                $ids = $db->insertMulti('users_skill', $dataSkills);
                if(!$ids) {
                    $db->rollback();
                    echo json_encode( array("status" => false,"info" => $db->getLastError(),"messages" => "Terjadi Kesalahan" ) );
                } else {
                    $db->commit();
                    echo json_encode( array("status" => true,"info" => "success","messages" => "Insert Success", "debug" => $id ) );
                }
            }

        }
        else
        {
            $db->rollback();
            echo json_encode( array("status" => false,"info" => $db->getLastError(),"messages" => $message ) );
        }
      
    }

}
catch (Exception $e) {
    $db->rollback();
    echo json_encode( array("status" => false,"info" => $e->getMessage(),"messages" => $e->getMessage() ) );

}


                
  
 

?>