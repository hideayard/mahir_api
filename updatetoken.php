<?php
ini_set('display_errors', 1);
// ini_set('display_startup_errors', none);
//error_reporting(E_ALL);
error_reporting(0);
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

require_once ("tokenlogin.php");
require_once ('config/MysqliDb.php');
include_once ("config/db.php");
require_once ("jwt_token.php");

$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
$token = isset($_POST['token']) ? str_replace('"','',$_POST['token']) : ""; 

if ($token != "") {

    try {

        $vtoken = json_decode( verify_token($token) );

        if($vtoken->status)
        {
          $db->where ('user_id', $vtoken->data->user_id);
          $results = $db->get('users');
          $secret = "B15m1ll4#";
          $otl = new TokenLogin($secret);

          $data = array($results[0]["user_id"]
                        ,$results[0]["user_name"]
                        ,$results[0]["user_nama"]
                        ,$results[0]["user_email"]
                        ,$results[0]["user_tipe"]
                        ,$results[0]["user_kelamin"]
                        ,$results[0]["user_hp"]
                        ,'uploads/user/'.$results[0]["user_foto"]
                        ,'uploads/user/'.$results[0]["user_ktp"]);
        //optional
        setcookie("token", $token, time() + (86400 * 30), "/"); // 86400 = 1 day
        $_SESSION['token']=$token; //id
        //--------- end optional
        $token = $otl->create_login_token($data[0],$data[1],$data[2],$data[3],$data[4],$data[5],$data[6],$data[7],$data[8]);
        echo json_encode( array("status" => true,"info" => $token,"messages" => "Token Updated.!" ) );



       } else {
        echo json_encode( array("status" => false,"info" => 'Invalid token',"messages" => "Invalid token!" ) );
       }
   } catch (Exception $e) {
      echo json_encode( array("status" => false,"info" => 'Caught exception '.$db->getMessage(),"messages" => "Terjadi Kesalahan!" ) );

   }
}
else
{
    echo json_encode( array("status" => false,"info" => "","messages" => "Token not found!" ) );
}

?>