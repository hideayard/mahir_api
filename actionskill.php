<?php

ini_set('display_errors', 0);
error_reporting(0);

// ini_set('display_startup_errors', none);
// //error_reporting(E_ALL);
// error_reporting(null);

session_start();

$token = isset($_POST['token']) ? str_replace('"','',$_POST['token']) : ""; 
try{

  // Check a POST is valid.
  if ($token != "") {
        
    require_once ("jwt_token.php");
    require_once ("customhelper.php");
      require_once ('config/MysqliDb.php');
      include_once ("config/db.php");
      $db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
      include("config/functions.php");    

      $vtoken = json_decode( verify_token($token) );
      $debug = array();    

      if($vtoken->status)
      {

        $id_session = $vtoken->data->user_id;
        $tipe_session = $vtoken->data->user_tipe;
        $target_dir = "uploads/skill/";
        $mode = isset($_POST['mode']) ? $_POST['mode'] : ""; 
        $tgl = (new \DateTime())->format('Y-m-d H:i:s');

        $id = isset($_POST['id']) ? $_POST['id'] : 0; 
        $metode_id = isset($_POST['metode_id']) ? $_POST['metode_id'] : 0; 
        $debug[] = "metode_id = ".$metode_id;    

        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : 0; 

        $sertifikat = isset($_FILES["sertifikat"]["name"]) ? $_FILES["sertifikat"]["name"] : ""; 
        $debug[] = "sertifikat=".$sertifikat;    

        $message = "Insert Sukses!!";
        $tgl = (new \DateTime())->format('Y-m-d H:i:s');
        $hasil_upload1 = null;
        
        $uploadOk =1 ;
      

        $data = Array (  );


        if($metode_id!="")
        {
            $data += array('metode_id' => $metode_id);
        }

        if($user_id!=0)
        {
            $data += array('user_id' => $user_id);
        }
        $i=-1;

        if($sertifikat != "" )
        { //harusnya foto lama di hapus dulu baru upload baru
            //upload_files (module/folder, nama_variable_foto, index)
          $hasil_upload1 = upload_files("sertifikat","sertifikat",++$i);
          $uploadOk .= "<br>".$hasil_upload1["uploadOk"];
          $message .= "<br>".$hasil_upload1["message"];
          $sertifikat = $hasil_upload1["file_name"];
          $data += array('sertifikat' => $hasil_upload1["file_name"]);
          $data += array('is_verified' => 1);
          $data += array('verified_by' => $id_session);
        }
      

        $hasil_eksekusi = false;

        if($id!=0)
        {
            if($mode == "delete" && $tipe_session=="ADMIN")
            {
              $db->where('id', $id);
              $hasil_eksekusi = $db->delete('users_skill');
              $message = "Delete Success !!";
            }
            else if($mode == "konfirmasi" && $tipe_session=="ADMIN")
            {
              $data += array('modified_by' => $id_session);
              $data += array('modified_at' => $tgl);
              $data += array('is_verified' => 1);
              $data += array('verified_by' => $id_session);
              
              $db->where('id', $id);
              $hasil_eksekusi = $db->update ('users_skill', $data);
              $message = "Konfirmasi Success !!";
            }
            else
            {
                //get nama
                $db->where ("status", 1);
                $db->where ('id', $metode_id );
                $getmetode = $db->get("metode");

                if(count($getmetode)>0)
                {
                    $data += array('nama' => $getmetode[0]["label"]);
                }
                //mode update
              $data += array('modified_by' => $id_session);
              $data += array('modified_at' => $tgl);
              if( $tipe_session=="ADMIN")
              {
                $db->where ('user_id', $user_id);
                $db->where ('id', $id);
                $debug[] = 'user_id'. $user_id;
                $debug[] = 'id'. $id;
              }
              else
              {
                $db->where ('user_id', $id_session);
                $db->where ('id', $id);
                $debug[] = 'user_id'. $id_session;
                $debug[] = 'id'. $id;
              }

              $hasil_eksekusi = $db->update ('users_skill', $data);
              $message = "Update Success !!";

            }
            
            if ($hasil_eksekusi)
            {   
              echo json_encode( array("status" => true,"info" => $message,"messages" => $message,"debug" => $debug ) );
            }//$db->count . ' records were updated';
            else
            {   
              echo json_encode( array("status" => false,"info" => 'update failed: ' . $db->getLastError(),"messages" => $message ,"debug" => $debug ) );

            }

          }
          else
          {  
              //cek dlu skillnya apakah sudah ada
              $db->where ('user_id', $id_session);
              $db->where ('metode_id', $metode_id);
              $cek = $db->get("users_skill");
                if( count($cek) < 1 )
                {
                    //mode insert
                    $message = "Insert Success";
                    $data += array("id" => null);
                    $data += array('created_by' => $id_session);
                    $data += array('created_at' => $tgl);
                    $data += array('modified_by' => $id_session);
                    $data += array('modified_at' => $tgl);
                    //get nama
                    $db->where ("status", 1);
                    $db->where ('id', $metode_id );
                    $results = $db->get("metode");

                        if(count($results)>0)
                        {
                            $data += array('nama' => $results[0]["label"]);

                            if($db->insert ('users_skill', $data))
                            {
                            echo json_encode( array("status" => true,"info" => $message,"messages" => $message ) );
                            }
                            else
                            {
                            $message = "Insert Fail";
                            echo json_encode( array("status" => false,"info" => $db->getLastError(),"messages" => $message ,"debug" => $debug ) );
                            }
                        }
                        else
                        {
                            $message = "terjadi kesalahan";
                            echo json_encode( array("status" => false,"info" => "Metode tidak ditemukan","messages" => $message ,"debug" => $debug ) );
                        } 
                }
                else
                {
                    echo json_encode( array("status" => false,"info" => "Skill Sudah Ada","messages" => "Skill Sudah Ada" ,"debug" => $debug ) );
                }
                          
           
          }
        }
        else
        {
          echo json_encode( array("status" => false,"info" => "token not valid","messages" => "token not valids!" ) );
        }
  }
  else
  {
    echo json_encode( array("status" => false,"info" => "request not valid","messages" => "request not valids!" ) );
  }
} catch (Exception $e) {
  echo json_encode( array("status" => false,"info" => "Terjadi Kesalahan","messages" => "Terjadi kesalahan" ,"e" => $e->getMessage() ) );

}
?>