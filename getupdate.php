<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', none);
// //error_reporting(E_ALL);
error_reporting(0);
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

      require_once ("tokenlogin.php");
      require_once ("jwt_token.php");
      require_once ("customhelper.php");
      require_once ('config/MysqliDb.php');
      include_once ("config/db.php");
      include("config/functions.php");    
// echo json_encode( array("status" => true,"info" => "data booking","messages" => "Success Get Data Booking!" ) );
// echo setlocale(LC_ALL,"ID");
$token = isset($_POST['token']) ? str_replace('"','',$_POST['token']) : ""; 

try{

    $db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);


    $vtoken = json_decode( verify_token($token) );

    $debug = array();    

      if($vtoken->status)
      {
        $id_session = $vtoken->data->user_id;
        $tipe_session = $vtoken->data->user_tipe;

        $limit = 10;
        $db->where("notif_to = 0 or notif_to=".$id_session);
        $notif = $db->get ("notif");//, $limit, "j.id, j.user_id as teacher_id, u.user_nama as pengajar, DATE_FORMAT(j.tanggal, '%d %M %Y') as tanggal, j.jam_mulai, j.jam_selesai, j.metode, (j.kuota - j.jml_peserta) as kuota , j.harga, j.status, j.url, j.can_edit" );
    
        // $jadwal = $db->get('jadwal'); //contains an Array of all users 
        $debug["id_session"]=$id_session;
        $debug[]=$notif;
        if(count($notif)>0)
        {
            echo json_encode( array("status" => true,"info" => $notif ,"messages" => "Get notif success!" ,"debug" =>  $debug ) );
        }
        else
        {
            echo json_encode( array("status" => true,"info" => [] ,"messages" => "Data tidak ditemukan!" ,"debug" =>  $debug ) );
        }
      }
      else
      {
        echo json_encode( array("status" => false,"info" => "Terjadi Kesalahan","messages" => "Token Not Valid" ) );

      }
      
} catch (Exception $e) {
    echo json_encode( array("status" => false,"info" => "Terjadi Kesalahan","messages" => "Terjadi kesalahan" ,"e" => $e->getMessage() ) );
  
  }
?>