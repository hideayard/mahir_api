<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', none);
//error_reporting(E_ALL);
error_reporting(0);
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

require_once ("tokenlogin.php");
require_once ('config/MysqliDb.php');
include_once ("config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);

$db->where ("user_id", 1);
$db->where ("status", 1);

$rekening = $db->get('users_bank_account',null,"nama_bank,no_rekening,atas_nama"); //contains an Array of all users 

if($rekening)
{
    $hasil = [];
    foreach ($rekening as $data) {
        $hasil[] = $data;
    }
    echo json_encode( array("status" => true,"info" => $hasil ,"messages" => "Get data success!" ) );
}
else
{
    echo json_encode( array("status" => false,"info" => 'Caught exception '.$db->getMessage() ,"messages" => "terjadi Kesalahan!" ) );

}

?>