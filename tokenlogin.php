<?php

require_once('vendors/autoload.php');
use \Firebase\JWT\JWT;

class TokenLogin {
   private $secret;

   function __construct($secret = "B15m1ll4#") {
      $this->secret = $secret;
   }

   function create_token($user_id, $token_duration = 24 /* Hours */) {
      $user_id = intval($user_id);
      if (!($user_id > 0)) return;

      $now = time();

      $data = array();
      $data['user_id'] = $user_id;
      $data['iat'] = $now;
      $data['exp'] = $now + $token_duration * (60 * 60);

      return JWT::encode($data, $this->secret);
   }

   function create_login_token($user_id,$user_name,$user_nama,$user_email,$user_tipe,$user_kelamin,$user_hp,$user_foto,$user_ktp, $token_duration = 24 /* Hours */) {
      $user_id = intval($user_id);
      if (!($user_id > 0)) return;

      $now = time();

      $data = array();
      $data['user_id'] = $user_id;
      $data['user_name'] = $user_name;
      $data['user_nama'] = $user_nama;
      $data['user_email'] = $user_email;
      $data['user_tipe'] = $user_tipe;
      $data['user_kelamin'] = $user_kelamin;
      $data['user_hp'] = $user_hp;
      $data['user_foto'] = $user_foto;
      $data['user_ktp'] = $user_ktp;
      $data['iat'] = $now;
      $data['exp'] = $now + $token_duration * (60 * 60) * 7;//d kali 7 karena token untuk 1 mgg

      return JWT::encode($data, $this->secret);
   }


   function validate_token($token) {
      try {
         $payload = JWT::decode($token, $this->secret, array('HS256'));
         if (!$payload) return FALSE;
         return json_decode(json_encode($payload));
      } catch (Exception $e) {
         // echo "token tidak valid";
         // die(var_dump($e));
         return FALSE;
      }
   }
}

?>