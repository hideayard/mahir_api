<?php

ini_set('display_errors', 0);
error_reporting(0);

// ini_set('display_startup_errors', none);
// //error_reporting(E_ALL);
// error_reporting(null);

session_start();

$token = isset($_POST['token']) ? str_replace('"','',$_POST['token']) : ""; 
try{

  // Check a POST is valid.
  if ($token != "") {
        
    require_once ("jwt_token.php");
    require_once ("customhelper.php");
      require_once ('config/MysqliDb.php');
      include_once ("config/db.php");
      $db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
      include("config/functions.php");    

      $vtoken = json_decode( verify_token($token) );
      $debug = array();    

      if($vtoken->status)
      {

        $id_session = $vtoken->data->user_id;
        $tipe_session = $vtoken->data->user_tipe;
        $target_dir = "uploads/rekening/";
        $mode = isset($_POST['mode']) ? $_POST['mode'] : ""; 
        $tgl = (new \DateTime())->format('Y-m-d H:i:s');

        $id = isset($_POST['id']) ? $_POST['id'] : 0; 
        $rekening_id = isset($_POST['rekening_id']) ? $_POST['rekening_id'] : 0; 
        $debug[] = "rekening_id = ".$rekening_id;    

        $nama_bank = isset($_POST['nama_bank']) ? $_POST['nama_bank'] : ""; 
        $no_rekening = isset($_POST['no_rekening']) ? $_POST['no_rekening'] : ""; 
        $atas_nama = isset($_POST['atas_nama']) ? $_POST['atas_nama'] : ""; 

        $user_id = $id_session;//isset($_POST['user_id']) ? $_POST['user_id'] : 0; 

        $attachment = isset($_FILES["attachment"]["name"]) ? $_FILES["attachment"]["name"] : ""; 
        $debug[] = "attachment=".$attachment;    

        $message = "Insert Sukses!!";
        $tgl = (new \DateTime())->format('Y-m-d H:i:s');
        $hasil_upload1 = null;
        
        $uploadOk =1 ;
      

        $data = Array (  );


        if($nama_bank!="")
        {
            $data += array('nama_bank' => $nama_bank);
        }
        if($no_rekening!="")
        {
            $data += array('no_rekening' => $no_rekening);
        }
        if($atas_nama!="")
        {
            $data += array('atas_nama' => $atas_nama);
        }

        if($attachment != "" )
        { //harusnya foto lama di hapus dulu baru upload baru
            //upload_files (module/folder, nama_variable_foto, index)
          $hasil_upload1 = upload_files("rekening","attachment",0);
          $uploadOk .= "<br>".$hasil_upload1["uploadOk"];
          $message .= "<br>".$hasil_upload1["message"];
          $attachment = $hasil_upload1["file_name"];
          $data += array('attachment' => $hasil_upload1["file_name"]);
          $data += array('status' => 1);
        }
      

        $hasil_eksekusi = false;

        if($id!=0)
        {
            if($mode == "delete")
            {
              if($tipe_session=="ADMIN")
              {
                $db->where('id', $id);
                $hasil_eksekusi = $db->delete('users_bank_account');
                $message = "Delete Success !!";
              }
              else{
                $hasil_eksekusi = false;
                $message = "Delete gagal, Anda tidak memiliki izin untuk hapus!";

              }
              
            }
            else if($mode == "verifikasi" && $tipe_session=="ADMIN")
            {
              $data = array('modified_by' => $id_session);
              $data += array('modified_at' => $tgl);
              $data += array('status' => 1);
              
              $db->where('id', $id);
              $hasil_eksekusi = $db->update ('users_bank_account', $data);
              $message = "verifikasi Success !!";
            }
            else
            {
               
                //mode update
              $data += array('modified_by' => $id_session);
              $data += array('modified_at' => $tgl);
              if( $tipe_session=="ADMIN")
              {
                $db->where ('user_id', $user_id);
                $db->where ('id', $id);
                $debug[] = 'user_id'. $user_id;
                $debug[] = 'id'. $id;
              }
              else
              {
                $db->where ('user_id', $id_session);
                $db->where ('id', $id);
                $debug[] = 'user_id'. $id_session;
                $debug[] = 'id'. $id;
              }

              $hasil_eksekusi = $db->update ('users_bank_account', $data);
              $message = "Update Success !!";

            }
            
            if ($hasil_eksekusi)
            {   
              echo json_encode( array("status" => true,"info" => $message,"messages" => $message,"debug" => $debug ) );
            }
            else
            {   
              echo json_encode( array("status" => false,"info" => 'update failed: ' . $db->getLastError(),"messages" => $message ,"debug" => $debug ) );

            }

          }
          else
          {  
              //cek dlu skillnya apakah sudah ada
              $db->where ('user_id', $id_session);
              $db->where ('no_rekening', $no_rekening);
              $cek = $db->get("users_bank_account");
                if( count($cek) < 1 )
                {
                    //mode insert
                    $message = "Insert Success";
                    $data += array("id" => null);
                    $data += array('created_by' => $id_session);
                    $data += array('created_at' => $tgl);
                    $data += array('modified_by' => $id_session);
                    $data += array('modified_at' => $tgl);
                    if($user_id!=0)
                    {
                        $data += array('user_id' => $user_id);
                    }
                    
                    if($db->insert ('users_bank_account', $data))
                    {
                    echo json_encode( array("status" => true,"info" => $message,"messages" => $message ) );
                    }
                    else
                    {
                    $message = "Insert Fail";
                    echo json_encode( array("status" => false,"info" => $db->getLastError(),"messages" => $message ,"debug" => $debug ) );
                    }
                       
                }
                else
                {
                    echo json_encode( array("status" => false,"info" => "Rekening Sudah Ada","messages" => "Rekening Sudah Ada" ,"debug" => $debug ) );
                }
                          
           
          }
        }
        else
        {
          echo json_encode( array("status" => false,"info" => "token not valid","messages" => "token not valids!" ) );
        }
  }
  else
  {
    echo json_encode( array("status" => false,"info" => "request not valid","messages" => "request not valids!" ) );
  }
} catch (Exception $e) {
  echo json_encode( array("status" => false,"info" => "Terjadi Kesalahan","messages" => "Terjadi kesalahan" ,"e" => $e->getMessage() ) );

}
?>