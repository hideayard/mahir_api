<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', none);
// //error_reporting(E_ALL);
error_reporting(0);
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

      require_once ("tokenlogin.php");
      require_once ("jwt_token.php");
      require_once ("customhelper.php");
      require_once ('config/MysqliDb.php');
      include_once ("config/db.php");
      include("config/functions.php");    
// echo json_encode( array("status" => true,"info" => "data booking","messages" => "Success Get Data Booking!" ) );
// echo setlocale(LC_ALL,"ID");
$token = isset($_POST['token']) ? str_replace('"','',$_POST['token']) : ""; 

try{

    $db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);

    $local = $db->rawQueryValue("SET lc_time_names = 'id_ID';");

    $vtoken = json_decode( verify_token($token) );

    $debug = array();    

      if($vtoken->status)
      {
        $id_session = $vtoken->data->user_id;
        $tipe_session = $vtoken->data->user_tipe;

      if($tipe_session != "ADMIN")
        { 
            $db->where ("j.user_id", $id_session);
            $db->where ("j.status > 0" );
        }

        $db->join("users u", "j.user_id = u.user_id", "INNER");
        $limit = 10;
        // $jadwal = $db->get ("jadwal j", $limit, "j.id, j.user_id as teacher_id, u.user_nama as pengajar, DATE_FORMAT(j.tanggal, '%d %M %Y') as tanggal, j.jam_mulai, j.jam_selesai, j.metode, (j.kuota - j.jml_peserta) as kuota,  j.jml_peserta , j.harga, j.status, j.url, j.can_edit" );
        $jadwal = $db->get ("jadwal j", $limit, "j.id, j.user_id as pengajar_id, u.user_nama as pengajar, DATE_FORMAT(j.tanggal, '%d-%m-%Y') as tgl, DATE_FORMAT(j.tanggal, '%d-%M-%Y') as tanggal, j.jam_mulai, j.jam_selesai, j.metode, (j.kuota - j.jml_peserta) as kuota,  j.jml_peserta, j.status, CASE WHEN (j.status = 2) THEN 'FINISHED' WHEN (j.status = 1) THEN 'ACTIVE' ELSE 'DELETED' END as status_text , j.can_edit" );
    
        // $jadwal = $db->get('jadwal'); //contains an Array of all users 
        $debug[]=$jadwal;
        if(count($jadwal)>0)
        {
            echo json_encode( array("status" => true,"info" => $jadwal ,"messages" => "Get data success!" ) );//,"debug" =>  $debug ) );
        }
        else
        {
            echo json_encode( array("status" => true,"info" => [] ,"messages" => "Data tidak ditemukan!" ) );//,"debug" =>  $debug ) );
        }
      }
      else
      {
        echo json_encode( array("status" => false,"info" => "Terjadi Kesalahan","messages" => "Token Not Valid" ) );

      }
      
} catch (Exception $e) {
    echo json_encode( array("status" => false,"info" => "Terjadi Kesalahan","messages" => "Terjadi kesalahan" ,"e" => $e->getMessage() ) );
  
  }
?>