<?php

ini_set('display_errors', 0);
error_reporting(0);

// ini_set('display_startup_errors', none);
// error_reporting(E_ALL);
// error_reporting(0);
// error_reporting(null);

session_start();

$token = isset($_POST['token']) ? str_replace('"','',$_POST['token']) : ""; 
try{

  // Check a POST is valid.
  if ($token != "") {
        
    require_once ("jwt_token.php");
    require_once ("customhelper.php");
    require_once ('config/MysqliDb.php');
    include_once ("config/db.php");
    $db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
    $db2 = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
    include("config/functions.php");    

      $vtoken = json_decode( verify_token($token) );

      if($vtoken->status)
      {

        $id_session = $vtoken->data->user_id;
        $tipe_session = $vtoken->data->user_tipe;
        $target_dir = "uploads/payment/";

        $order_id = isset($_POST['order_id']) ? $_POST['order_id'] : 0; 

        $mode = isset($_POST['mode']) ? $_POST['mode'] : ""; 

        $tgl = (new \DateTime())->format('Y-m-d H:i:s');

        $payment_bukti = isset($_FILES["payment_bukti"]["name"]) ? $_FILES["payment_bukti"]["name"] : ""; 

        $message = "Insert Sukses!!";
        $tgl = (new \DateTime())->format('Y-m-d H:i:s');
        $hasil_upload1 = null;
        
        $uploadOk =1 ;
      

        $data = Array (  );
        if($order_id!=0)
        {
            $data += array('id' => intval($order_id));
        }

        if($payment_bukti != "" )
        { //harusnya foto lama di hapus dulu baru upload baru
          $hasil_upload1 = upload_files("payment","payment_bukti",0);
          $uploadOk .= "<br>".$hasil_upload1["uploadOk"];
          $message .= "<br>".$hasil_upload1["message"];
          $payment_bukti = $hasil_upload1["file_name"];
          $data += array('payment_bukti' => $hasil_upload1["file_name"]);
        }

        $hasil_eksekusi = false;

        if(isset($_POST['order_id']))
        {    
          if($mode == "delete" && $tipe_session=="ADMIN")
            {
              $db->where('user_id', $user_id);
              $hasil_eksekusi = $db->delete('users');
              $message = "Delete Success !!";
            }
            else
            {
              $data += array('modified_by' => $id_session);
              $data += array('modified_at' => $tgl);
              $data += array('is_deleted' => 0);
              if( $tipe_session=="ADMIN")
              {
                $db->where ('id', $order_id);
                $db2->where ('id', $order_id);
              }
              else
              {
                $db->where ('user_id', intval($id_session));
                $db->where ('id', intval($order_id));
                $db2->where ('user_id', intval($id_session));
                $db2->where ('id', intval($order_id));
              }
              // echo $id_session. "-".$order_id;
              $order = $db2->get("booking",1,"id, status, remark, invoice, payment_bukti" );

              if(count($order) > 0)
              {
                if($order[0]["status"] != "FINISHED")
                {
                    if($tipe_session=="ADMIN" || ($payment_bukti != "" ))
                    {
                        $data += array('status' => "PAYMENT VERIFIED");
                        $data += array('remark' => "Pembayaran sudah di verifikasi, link dapat dilihat di menu order");       
             
                    }
                    else
                    {
                        $data += array('status' => "PAYMENT VERIFICATION");
                        $data += array('remark' => "MOHON TUNGGU PROSES PAYMENT VERIFICATION (max.1x24 jam)");
                    }

                }

                // var_dump($data);
                // var_dump($db);
                $hasil_eksekusi = $db->update('booking', $data);
                if ($hasil_eksekusi)
                {   
                  echo json_encode( array("status" => true,"info" => $data["status"],"messages" => "Update Success !!" ) );
                }//$db->count . ' records were updated';
                else
                {   
                  echo json_encode( array("status" => false,"info" => 'update failed: ' . $db->getLastError(),"messages" => "Update Failed !!" ) );

                }


              }
              else
              {
                echo json_encode( array("status" => false,"info" => "data not found","messages" => "data not found" ) );
              }

              

            }
            
            

          }
          else
          {  
            $message = "Insert Success";
              $data += array("id" => null);
              $data += array('created_by' => $id_session);
              $data += array('created_at' => $tgl);
            if($db->insert ('bopoking', $data))
            {
              echo json_encode( array("status" => true,"info" => $user_status,"messages" => $message ) );
                      }
            else
            {
              $message = "Insert Fail";
              echo json_encode( array("status" => false,"info" => $db->getLastError(),"messages" => $message ) );
          
          
            }
          }
        }
        else
        {
          echo json_encode( array("status" => false,"info" => "token not valid","messages" => "token not valids!" ) );
        }
  }
  else
  {
    echo json_encode( array("status" => false,"info" => "request not valid","messages" => "request not valids!" ) );
  }
} catch (Exception $e) {
  echo json_encode( array("status" => false,"info" => "Terjadi Kesalahan","messages" => "Terjadi kesalahan" ,"e" => $e->getMessage() ) );

}
?>