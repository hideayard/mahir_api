<?php

// ini_set('display_errors', 0);
// error_reporting(0);

// ini_set('display_startup_errors', none);
//error_reporting(E_ALL);
// error_reporting(0);
// error_reporting(null);

session_start();

$token = isset($_POST['token']) ? str_replace('"','',$_POST['token']) : ""; 
$data = Array ();
$ran = str_pad(mt_rand(0, 999), 3, '0', STR_PAD_LEFT);

try{

  // Check a POST is valid.
  if ($token != "") {
        
    require_once ("jwt_token.php");
    require_once ("customhelper.php");
      require_once ('config/MysqliDb.php');
      include_once ("config/db.php");
      $db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
      include("config/functions.php");    

      $vtoken = json_decode( verify_token($token) );
      $debug = array();    

      if($vtoken->status)
      {
        $db->startTransaction();

        $id_session = $vtoken->data->user_id;
        $tipe_session = $vtoken->data->user_tipe;

        $jadwal_id = isset($_POST['jadwal']) ? $_POST['jadwal'] : 0; 
        $id = isset($_POST['id']) ? $_POST['id'] : 0; 
        $tgl = (new \DateTime())->format('Y-m-d H:i:s');

        $debug[] = "jadwal = ".$jadwal_id;    

        $message = "Booking Sukses!!";
        $tgl = (new \DateTime())->format('Y-m-d H:i:s');        

        $hasil_eksekusi = false;

        if($id!=0)
        {
            if($mode == "delete" && $tipe_session=="ADMIN")
            {
              $db->where('jadwal_id', $jadwal_id);
              $db->where('user_id', $id_session);
              $hasil_eksekusi = $db->delete('booking');
              $message = "Delete Success !!";
            }
            else
            {
                //get nama
                $db->where ("status", 1);
                $db->where ('id', $id );
                $getbooking = $db->get("booking");

                if(count($getbooking)>0)
                {
                    $data += array('nama' => $getbooking[0]["label"]);
                }
                //mode update
              $data += array('modified_by' => $id_session);
              $data += array('modified_at' => $tgl);
              if( $tipe_session=="ADMIN")
              {
                $db->where ('user_id', $user_id);
                $db->where ('id', $id);
                $debug[] = 'user_id'. $user_id;
                $debug[] = 'id'. $id;
              }
              else
              {
                $db->where ('user_id', $id_session);
                $db->where ('id', $id);
                $debug[] = 'user_id'. $id_session;
                $debug[] = 'id'. $id;
              }

              $hasil_eksekusi = $db->update ('users_skill', $data);
              $message = "Update Success !!";

            }
            
            if ($hasil_eksekusi)
            {   
              echo json_encode( array("status" => true,"info" => $message,"messages" => $message,"debug" => $debug ) );
            }//$db->count . ' records were updated';
            else
            {   
              echo json_encode( array("status" => false,"info" => 'update failed: ' . $db->getLastError(),"messages" => $message ,"debug" => $debug ) );

            }

          }
          else
          {  
            //cek dlu jml kuota
            $db->where ('jml_peserta < kuota');
            $db->where ('id', $jadwal_id);
            $jmlkuota = $db->getValue ("jadwal", "count(*)");

            if($jmlkuota > 0)
            {
              //cek dlu skillnya apakah sudah ada
              $db->where ('user_id', $id_session);
              $db->where ('jadwal_id', $jadwal_id);
              $cek = $db->get("booking");
                if( count($cek) < 1 )
                {
                    //mode insert
                    $message = "Booking Success";
                    $data += array("id" => null);
                    $data += array('jadwal_id' => $jadwal_id);
                    $data += array('user_id' => $id_session);
                    $data += array('status' => "BOOKED");
                    $data += array('invoice' => "INV/".$jadwal_id."/".$id_session."/".$ran);
                    $data += array('expired' => ( new \DateTime())->modify('+1 day')->format('Y-m-d H:i:s') );
                    $data += array('remark' => "MOHON UNTUK MELAKUKAN PEMBAYARAN SEBELUM TANGGAL ".(new \DateTime())->modify('+1 day')->format('Y-m-d H:i:s'));
                    $data += array('created_by' => $id_session);
                    $data += array('created_at' => $tgl);
                    $data += array('modified_by' => $id_session);
                    $data += array('modified_at' => $tgl);
                    
                    $debug[]= $data;

                    if($db->insert ('booking', $data))
                    {
                      $db->where ('id', intval($jadwal_id) );
                      $jml_peserta = $db->getValue ("jadwal", "jml_peserta");
                      $debug[]= $jml_peserta;

                      $data = array("can_edit" => 0);
                      $data += array("jml_peserta" => ++$jml_peserta);
                      $debug["data sblm update"]= $data;
                      ///shere lg
                      $db->where ('id', intval($jadwal_id) );
                      $hasil_eksekusi = $db->update ('jadwal', $data);

                      if( $hasil_eksekusi ) 
                      {
                        $db->commit();
                        echo json_encode( array("status" => true,"info" => $message,"messages" => $message ,"debug" => $debug ) );
                      }
                      else
                      {
                        $db->rollback();
                        $message = "Booking Fail - cannot update jadwal";
                        echo json_encode( array("status" => false,"info" => $db->getLastError(),"messages" => $message ,"debug" => $debug ) );
                      }
                    }
                    else
                    {
                      $db->rollback();
                      $message = "Booking Fail";
                      echo json_encode( array("status" => false,"info" => $db->getLastError(),"messages" => $message ,"debug" => $debug ) );
                    }
                
                }
                else
                {
                    echo json_encode( array("status" => false,"info" => "Jadwal Sudah di booking","messages" => "Data Booking Sudah Ada" ,"debug" => $debug ) );
                }
              }
              else{
                echo json_encode( array("status" => false,"info" => "Kuota Sudah Penuh","messages" => "Kuota Sudah Penuh" ,"debug" => $debug ) );
              }
                          
           
          }
        }
        else
        {
          echo json_encode( array("status" => false,"info" => "token not valid","messages" => "token not valids!" ) );
        }
  }
  else
  {
    echo json_encode( array("status" => false,"info" => "request not valid","messages" => "request not valids!" ) );
  }
} catch (Exception $e) {
  $db->rollback();
  echo json_encode( array("status" => false,"info" => "Terjadi Kesalahan","messages" => "Terjadi kesalahan" ,"e" => $e->getMessage() ) );

}
?>