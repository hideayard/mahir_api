<?php

ini_set('display_errors', 0);
error_reporting(0);

// ini_set('display_startup_errors', none);
// //error_reporting(E_ALL);
// error_reporting(null);

session_start();

$token = isset($_POST['token']) ? str_replace('"','',$_POST['token']) : ""; 
try{

  // Check a POST is valid.
  if ($token != "") {
        
    require_once ("jwt_token.php");
    require_once ("customhelper.php");
    require_once ('config/MysqliDb.php');
    include_once ("config/db.php");
    $db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
    $db2 = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
    include("config/functions.php");    

      $vtoken = json_decode( verify_token($token) );
      $debug = array();    

      if($vtoken->status)
      {

        $id_session = $vtoken->data->user_id;
        $tipe_session = $vtoken->data->user_tipe;

        $mode = isset($_POST['mode']) ? $_POST['mode'] : ""; 
        $tgl = (new \DateTime())->format('Y-m-d');

        $id = isset($_POST['id']) ? $_POST['id'] : 0; 
        $metode_id = isset($_POST['metode_id']) ? $_POST['metode_id'] : 0; 
        $tanggal = isset($_POST['tanggal']) ? $_POST['tanggal'] : $tgl; 
        $mulai = isset($_POST['mulai']) ? $_POST['mulai'] : 0; 
        $selesai = isset($_POST['selesai']) ? $_POST['selesai'] : 0; 
        $kuota = isset($_POST['kuota']) ? $_POST['kuota'] : 0; 
        $url = isset($_POST['url']) ? $_POST['url'] : ""; 

        $message = "Insert Sukses!!";

        $data = Array (  );

        if($metode_id!="")
        {
            $data += array('id_metode' => $metode_id);
        }
        if($url!="")
        {
            $data += array('url' => $url);
        }
        if($id_session!=0)
        {
            $data += array('user_id' => $id_session);
        }
        if($mulai!=0)
        {
            $data += array('jam_mulai' => $mulai);
        }
        if($selesai!=0)
        {
            $data += array('jam_selesai' => $selesai);
        }
        if($kuota!=0)
        {
            $data += array('kuota' => $kuota);
        }

        $harga = (abs($selesai - $mulai) )* 30000;
        $data += array('harga' => $harga);

        $hasil_eksekusi = false;
        if($id!=0)
        {
            $db->where('id', $id);
            if($mode == "finish")
            {
                if($tipe_session!="ADMIN")
                {
                    $db->where('user_id', $id_session);

                }
                $data = array('status' => 2);
                $hasil_eksekusi = $db->update ('jadwal', $data);
                $message = "Jadwal Finished !!";
            }
            elseif($mode == "delete")
            {
                if($tipe_session!="ADMIN")
                {
                    $db->where('user_id', $id_session);

                }
                $data = array('status' => 0);
                $hasil_eksekusi = $db->update ('jadwal', $data);
                $message = "Delete Success !!";
            }
            else
            {
                 //cek dlu metodenya apakah sudah ada
                $db2->where ('id', $metode_id);
                $metode = $db2->get("metode");
                $debug[] = $metode;
                if(count($metode) > 0 )
                {
                    if($tanggal!="")
                    {
                        $data += array('tanggal' => $tanggal);
                    }
                    //mode update
                    $data += array('metode' => $metode[0]["nama"]);
                    $data += array('modified_by' => $id_session);
                    $data += array('modified_at' => $tgl);
                    if( $tipe_session=="ADMIN")
                    {
                        $db->where ('user_id', $user_id);
                        $db->where ('id', $id);
                        $debug[] = 'user_id'. $user_id;
                        $debug[] = 'id'. $id;
                    }
                    else
                    {
                        $db->where ('user_id', $id_session);
                        $db->where ('id', $id);
                        $debug[] = 'user_id'. $id_session;
                        $debug[] = 'id'. $id;
                    }

                    $hasil_eksekusi = $db->update ('jadwal', $data);
                    $message = "Update Success !!";
                }
                else
                {
                    echo json_encode( array("status" => false,"info" => "Metode Tidak Ditemukan","messages" => "Metode Tidak Ditemukan" ,"debug" => $debug ) );
                }

            }
            
            if ($hasil_eksekusi)
            {   
              echo json_encode( array("status" => true,"info" => $message,"messages" => $message,"debug" => $debug ) );
            }//$db->count . ' records were updated';
            else
            {   
              echo json_encode( array("status" => false,"info" => 'update failed: ' . $db->getLastError(),"messages" => $message ,"debug" => $debug ) );

            }

          }
          else
          {  
            //cek dlu metodenya apakah sudah ada
            $db->where ('id', $metode_id);
            $metode = $db->get("metode");
            $debug[] = $metode;

            if(count($metode) > 0 )
            {
                //cek dlu jadwalnya apakah sudah ada
                if($tanggal!="")
                {
                    $db->where ("DATE(tanggal) = DATE('".$tanggal."')");
                    $debug[]=$tanggal;
                    $data += array('tanggal' => $tanggal);
                }


                if($mulai!="" && $mulai!=0)
                {
                    $debug[]=$mulai;
                    $db->where ('jam_mulai', $jam, "=");
                }

                if($selesai!="" && $selesai!=0)
                {
                    $debug[]=$selesai;
                    $db->where ('jam_selesai', $jam, "=");
                }
                $db->where ('user_id', $id_session);
                $db->where ('id_metode', $metode_id);
                $cek = $db->get("jadwal");
                $harga = (abs($selesai - $mulai) )* 30000;
                $data += array('harga' => $harga);

                if( count($cek) < 1 )
                {
                    //mode insert
                    $message = "Insert Success";
                    $data += array("id" => null);
                    $data += array('metode' => $metode[0]["nama"]);
                    $data += array('created_by' => $id_session);
                    $data += array('created_at' => $tgl);
                    $data += array('modified_by' => $id_session);
                    $data += array('modified_at' => $tgl);
                    $data += array('status' => 1);

                    if($db->insert ('jadwal', $data))
                    {
                        echo json_encode( array("status" => true,"info" => $message,"messages" => $message ) );
                    }
                    else
                    {
                        $message = "Insert Fail";
                        echo json_encode( array("status" => false,"info" => $db->getLastError(),"messages" => $message ,"debug" => $debug ) );
                    }

                }
                else
                {
                    echo json_encode( array("status" => false,"info" => "Jadwal Sudah Ada","messages" => "Jadwal Sudah Ada" ,"debug" => $debug ) );
                }
            }
            else
            {
                echo json_encode( array("status" => false,"info" => "Metode Tidak Ditemukan","messages" => "Metode Tidak Ditemukan" ,"debug" => $debug ) );
            }
              
                          
           
          }
        }
        else
        {
          echo json_encode( array("status" => false,"info" => "token not valid","messages" => "token not valids!" ) );
        }
  }
  else
  {
    echo json_encode( array("status" => false,"info" => "request not valid","messages" => "request not valids!" ) );
  }
} catch (Exception $e) {
  echo json_encode( array("status" => false,"info" => "Terjadi Kesalahan","messages" => "Terjadi kesalahan" ,"e" => $e->getMessage() ) );

}
?>