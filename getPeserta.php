<?php
ini_set('display_errors', 1);
// ini_set('display_startup_errors', none);
//error_reporting(E_ALL);
error_reporting(0);
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

require_once ("tokenlogin.php");
require_once ('config/MysqliDb.php');
include_once ("config/db.php");
require_once ("jwt_token.php");

$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
$token = isset($_POST['token']) ? $_POST['token'] : ""; 
$jadwal_id = isset($_POST['jadwal_id']) ? intval($_POST['jadwal_id']) : 0; 

$page = isset($_POST['page']) ? intval($_POST['page']) : 1; 
$totalPages = 1;
if ($token != "") {


    try {

        $vtoken = json_decode( verify_token($token) );

        if($vtoken->status)
        {

            $id_session = $vtoken->data->user_id;
            $tipe_session = $vtoken->data->user_tipe;
       
            $db->where ("b.jadwal_id", $jadwal_id);
            $limit = NULL;
            ////sub query

            $db->join("users u", "b.user_id=u.user_id", "INNER");
            $db->join("jadwal j", "b.jadwal_id=j.id", "INNER");

            $peserta = $db->get ("booking b", $limit, "u.user_nama, u.user_name, b.status, b.jadwal_id" );

        ///======================================

            if(count($peserta)>0)
            {
                echo json_encode( array("status" => true,"info" => $peserta ,"messages" => "Get Data Success" ) );
            }
            else
            {
                echo json_encode( array("status" => false,"info" => "No Data Found.!" ,"messages" => "No Data Found.!" ) );
            }

       } else {
        echo json_encode( array("status" => false,"info" => 'Invalid token',"messages" => "Invalid token!" ) );
       }
   } catch (Exception $e) {
      echo json_encode( array("status" => false,"info" => 'Caught exception '.$db->getMessage(),"messages" => "Terjadi Kesalahan!" ) );

   }
}
else
{
    echo json_encode( array("status" => false,"info" => "","messages" => "Token not found!" ) );
}

?>