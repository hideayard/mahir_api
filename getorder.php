<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', none);
// //error_reporting(E_ALL);
error_reporting(0);
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

      require_once ("tokenlogin.php");
      require_once ("jwt_token.php");
      require_once ("customhelper.php");
      require_once ('config/MysqliDb.php');
      include_once ("config/db.php");
      include("config/functions.php");    
// echo json_encode( array("status" => true,"info" => "data booking","messages" => "Success Get Data Booking!" ) );
// echo setlocale(LC_ALL,"ID");
$token = isset($_POST['token']) ? str_replace('"','',$_POST['token']) : ""; 

try{

    $db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);

    $local = $db->rawQueryValue("SET lc_time_names = 'id_ID';");

    $vtoken = json_decode( verify_token($token) );

    $debug = array();    
    $db->setTrace (true);

      if($vtoken->status)
      {
        $id_session = $vtoken->data->user_id;
        $tipe_session = $vtoken->data->user_tipe;

        if($tipe_session == "ADMIN")
        { 
        }
        else if($tipe_session == "USTADZ")
        { 
          $db->where ("vj.teacher_id", $id_session);
          $db->where ("b.status IN ('FINISHED','PAYMENT VERIFIED')");
        }
        else if($tipe_session != "ADMIN")
        { 
          $db->where ("b.user_id", $id_session);
        }

        // $db->join("users u", "j.user_id=u.user_id", "INNER");
        // $db->join("jadwal j", "b.jadwal_id=j.id", "INNER");
        $limit = 10;
        ////sub query
        $sub = $db->subQuery ("vj");
        $sub->join("users u", "j.user_id=u.user_id", "INNER");
        $sub->get ("jadwal j", null, "u.user_nama,u.user_id as teacher_id, j.id,DATE_FORMAT(j.tanggal, '%d %M %Y') as tanggal, j.jam_mulai, j.jam_selesai, j.metode, j.kuota, j.jml_peserta, j.harga, j.status, j.url" );
        $db->join($sub, "b.jadwal_id=vj.id", "LEFT");
        $db->join("users us", "us.user_id=b.user_id", "INNER");

        $order = $db->get ("booking b", $limit, "us.user_nama as nama_peserta, vj.teacher_id,vj.user_nama as pengajar, b.id, vj.tanggal, vj.jam_mulai, vj.jam_selesai, vj.metode, (vj.kuota - vj.jml_peserta) as kuota , vj.harga, b.status, b.remark, b.invoice, b.payment_id, b.payment_bukti, CASE WHEN (b.status = 'FINISHED' or b.status = 'PAYMENT VERIFIED') THEN vj.url ELSE '-' END as url" );
        // $debug[]=$db->trace;
        if(count($order)>0)
        {
          //expired pindah ke cron
          ///---------check data expired 
          // $db->where ( "expired < DATE_FORMAT('".(new \DateTime())->format('Y-m-d H:i:s') ."', '%Y-%m-%d %H:%i:%s')" );
          // $db->where ( "status <> 'EXPIRED'" );
          // $data = Array (
          //   'status' => 'EXPIRED',
          //   'remark' => 'DATA BOOKING TELAH EXPIRED, ANDA DAPAT BOOKING ULANG ATAU MENGHUBUNGI ADMIN UNTUK INFO LEBIH LANJUT',
          // );
          // $getexpired = $db->get('booking');
          // //debug
          // $debug[]=$getexpired;
          // if ($db->update ('booking', $data))
          // {
          //   $jml = $db->count;
          //   //debug
          //   $debug[]=$jml;
          //   if($jml >0)
          //   {
          //     foreach($getexpired as $key => $value)
          //     {
          //       $q_jadwal = $db->rawQuery('UPDATE jadwal set jml_peserta=jml_peserta+1 where id = ?', Array ($value["jadwal_id"]));
          //     }
          //   }
          //   $expired =  $jml . ' data expired';
          // }
          // else
          //  {
          //    $expired =  'update failed: ' . $db->getLastError();
          //  } 
        /// --end check data expired
            echo json_encode( array("status" => true,"info" => $order ,"messages" => "Get data success!"  ,"debug" =>  $debug ) );
        }
        else
        {
            echo json_encode( array("status" => true,"info" => [] ,"messages" => "Belum ada order!" ,"debug" =>  $debug ) );
        }
      }
      else
      {
        echo json_encode( array("status" => false,"info" => "Terjadi Kesalahan","messages" => "Token Not Valid" ) );

      }
      
} catch (Exception $e) {
    echo json_encode( array("status" => false,"info" => "Terjadi Kesalahan","messages" => "Terjadi kesalahan" ,"e" => $e->getMessage() ) );
  
  }
?>